FROM telegraf:1.23.4-alpine

# COPY config/telegraf.conf /etc/telegraf/telegraf.conf

# RUN mkdir /keys
# COPY keys/bezvanet-ssh-key /keys/bezvanet-ssh-key
# RUN chmod 600 /keys/*

# RUN mkdir /commands
# COPY commands/* /commands/
# RUN chmod +x /commands/*

RUN setcap cap_net_raw+ep /usr/bin/telegraf
RUN apk update &&  \
    apk add --no-cache curl jq bash git coreutils zip openssh-client &&  \
    rm -rf /var/cache/apk/*
RUN setcap cap_net_raw+ep /usr/bin/telegraf

ENTRYPOINT ["/usr/bin/telegraf","--config","/etc/telegraf/telegraf.conf","--config-directory","/etc/telegraf/telegraf.d"]
