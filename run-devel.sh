#!/usr/bin/env bash
podman run --rm -ti --name telegraf-devel \
    -v $(PWD)/tests/config/telegraf.conf:/etc/telegraf/telegraf.conf:ro \
    -v $(PWD)/tests/example-data/input.json:/input.json:ro \
    registry.gitlab.com/mr-dave007/telegraf:latest
